#include <iostream>
#include "MessagesSender.h"

#define OPTION_EXIT 4

int  main(void)
{
	MessagesSender myClass;
	int optionUser = 0;

	std::thread t1 (&MessagesSender::readFromText ,ref(myClass));
	std::thread t2 (&MessagesSender::sendMessages, ref(myClass));
	
	while (optionUser != OPTION_EXIT)
	{
		cout << " -------------------" << endl;
		cout << "|1 Signin           |" << endl;
		cout << "|2.Signout          |" << endl;
		cout << "|3.Connected Users  |" << endl;
		cout << "|4.exit             |\n -------------------" << endl;
		cin >> optionUser;
		
		switch (optionUser)
		{
		case 1:
			myClass.signin();
			break;
		case 2:
			myClass.Signout();
			break;
		case 3:
			myClass.ConnectedUsers();
			break;
		case 4:
			cout << "finish" << endl;
			break;
		default:
			cout << "Error, must be between 1-4" << endl;
			cout << "Enter your choice again" << endl;
			break;
		}
	}
	t1.detach();
	t2.detach();
	
	return 0;
}