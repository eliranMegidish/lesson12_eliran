#pragma once
#include <vector> 
#include <iostream> 
#include <algorithm>
#include <fstream>
#include <string>
#include <thread>
#include <queue> 
#include <queue> 
#include <mutex>           
#include <condition_variable> 

using namespace std;

class MessagesSender 
{

public:
	MessagesSender();
	void signin();
	void Signout();
	void ConnectedUsers();
	void readFromText();
	void sendMessages();
	

private:
	vector<string> _users;
	queue <string> *_messages; //pointer to queue - because when i call thread i made chane in this value and the change not svae 
							  //because thread's memory in the heap not in stack 
	mutex _mtx;
	condition_variable _cv;

};
