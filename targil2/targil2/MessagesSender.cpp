#include "MessagesSender.h"

#define NAME_FILE_READ "data.txt"
#define NAME_FILE_WRITE "output.txt"
#define TIME_TO_SLIP 10


MessagesSender::MessagesSender()
{
	this->_messages = new queue<string>;
}

void MessagesSender::signin()
{
	string userName;
	cout << "Enter user name" << endl;
	cin >> userName;
	if (find(this->_users.begin(), this->_users.end(), userName) != this->_users.end())
	{
		cout <<"Element in vector."<<endl;

	}
	else // Element not in vector.
	{
		this->_users.push_back(userName);
	}
}


void MessagesSender::Signout()
{
	int i = 0;
	string userName;
	std::vector<string>::iterator it;

	cout << "Enter user name to sign out" << endl;
	cin >> userName;

	if (find(this->_users.begin(), this->_users.end(), userName) != this->_users.end())
	{
		for (it = this->_users.begin(); it != this->_users.end(); ++it)
		{

			if (userName.compare(*it) == 0)
			{
				this->_users.erase(this->_users.begin()+i);
				break;
			}
			i++;
		}
	}
	else
	{
		cout <<"Not in list users"<<endl;
	}
}

void MessagesSender::ConnectedUsers()
{

	std::vector<string>::iterator it;
	cout << "Print vector \n" << endl;
	for (it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		cout << *it << endl;
	}
}


void MessagesSender::readFromText()
{
	while (true)
	{
		string line;
		fstream  myFile(NAME_FILE_READ);

		if (myFile.is_open())
		{
			while (getline(myFile, line))
			{
				this->_messages->push(line);
			}

			ofstream  myFile2(NAME_FILE_READ);
			this_thread::sleep_for(std::chrono::seconds(TIME_TO_SLIP));
			myFile2.close();
			unique_lock<std::mutex> lck(this->_mtx);
			this->_cv.notify_all();
		}
		myFile.close();
	}
}

void MessagesSender::sendMessages()
{	
	int j = 0;

	while (true)
	{
		fstream myFile;
		myFile.open(NAME_FILE_WRITE, ios::out | ios::in);

		string s;

		std::unique_lock<std::mutex> lck(this->_mtx);
		this->_cv.wait(lck);

		while (!this->_messages->empty() && this->_users.size()!=0)
		{
			for (j = 0; j < this->_users.size(); j++)
			{
				//cout << this->_users[j] << ": " << this->_messages->front() << endl;//print to all messages to screen
				myFile << this->_users[j] << ": " << this->_messages->front() << endl;
			}
			this->_messages->pop();
		}
		myFile.close();
		this_thread::sleep_for(std::chrono::seconds(1));
	}
}


