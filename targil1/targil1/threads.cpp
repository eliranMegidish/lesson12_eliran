#include "threads.h"

using namespace std;
std::mutex mtx;


bool checkPrime(int num)
{
	if (num <= 1)
		return false;
	for (int i = 2; i <= num / 2; i++)
	{
		if (num % i == 0)
		{
			return false;
		}
	}
	return true;
}


void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int count = 0;
	for (int i = begin; i <= end; i++)
	{
		if (checkPrime(i))
		{
			mtx.lock();
			file << i << endl;
			mtx.unlock();
		}
	}
}


void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream myFile(filePath);
	clock_t start, finish;
	double time_spent;


	start = clock();

	int amount = (end - begin) / N + 1;
	std::thread* arr = new std::thread[N];
	for (int i = 0; i < N; i++)
	{
		int new_end = begin + amount * (i + 1) - 1 > end ? end : begin + amount * (i + 1) - 1;
		arr[i] = std::thread(writePrimesToFile, begin + amount * i, new_end, ref(myFile));
	}
	for (int i = 0; i < N; i++)
	{
		arr[i].join();
	}

	finish = clock();
	time_spent = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << begin << "-" << end << std::setprecision(5) << ": " << time_spent << " seconds" << endl;

	myFile.close();
}

