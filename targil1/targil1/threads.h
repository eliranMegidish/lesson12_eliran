#pragma once


#include <fstream>
#include "threads.h"
#include <thread>  
#include <time.h>
#include <iomanip> 
#include <chrono> 
#include <iostream> 
#include <mutex> 

using namespace std;



void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);